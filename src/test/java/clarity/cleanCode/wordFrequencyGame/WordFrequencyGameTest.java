package clarity.cleanCode.wordFrequencyGame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class WordFrequencyGameTest {
    WordFrequencyGame game;
    @BeforeEach
    public void init(){
         game = new WordFrequencyGame();
    }

    @Test
    public void should_get_the_1_when_input_the() throws Exception {
        
        String inputStr = "the";
        String expectResult = "the 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }

    @Test
    public void should_process_two_words() throws Exception {
        
        String inputStr = "the is";
        String expectResult = "the 1\nis 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }

    @Test
    public void should_process_two_words_with_special_spaces() throws Exception {
        
        String inputStr = "the      is";
        String expectResult = "the 1\nis 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }

    @Test
    public void should_process_two_words_with_special_enter() throws Exception {
        
        String inputStr = "the   \n   is";
        String expectResult = "the 1\nis 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }

    @Test
    public void should_pracess_two_same_words_with_sorted() throws Exception {
        
        String inputStr = "the the is";
        String expectResult = "the 2\nis 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }

    @Test
    public void should_process_sorted_with_count_descending() throws Exception {
        
        String inputStr = "the is is";
        String expectResult = "is 2\nthe 1";
        
        String result = game.getResult(inputStr);
        
        assertThat(result, is(expectResult));
    }
}
