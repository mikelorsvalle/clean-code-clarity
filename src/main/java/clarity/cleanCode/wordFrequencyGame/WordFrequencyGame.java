package clarity.cleanCode.wordFrequencyGame;


import java.util.*;

public class WordFrequencyGame {
    public String getResult(String inputStr) {

        if (inputStr.split("\\s+").length == 1) {
            return inputStr + " 1";
        } else {

            try {

                //split the input cadena with 1 to n pieces of spaces
                String[] arr = inputStr.split("\\s+");

                List<Input> inputList = new ArrayList<>();
                for (String s : arr) {
                    Input input = new Input(s, 1);
                    inputList.add(input);
                }

                //get the map for the next step of sizing the same word
                Map<String, List<Input>> map = new HashMap<>();
                for (Input input : inputList) {
                    //       map.computeIfAbsent(input.getValue(), k -> new ArrayList<>()).add(input);
                    if (!map.containsKey(input.getValue())) {
                        ArrayList ar = new ArrayList<>();
                        ar.add(input);
                        map.put(input.getValue(), ar);
                    } else {
                        map.get(input.getValue()).add(input);
                    }
                }

                List<Input> list = new ArrayList<>();
                for (Map.Entry<String, List<Input>> entry : map.entrySet()) {
                    Input input = new Input(entry.getKey(), entry.getValue().size());
                    list.add(input);
                }
                inputList = list;

                inputList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());

                StringJoiner joiner = new StringJoiner("\n");
                for (Input w : inputList) {
                    String s = w.getValue() + " " + w.getWordCount();
                    joiner.add(s);
                }
                return joiner.toString();
            } catch (Exception e) {
                return "Calculate Error";
            }
        }
    }
}

